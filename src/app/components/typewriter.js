import React, { useEffect, useState } from 'react';

const Typewriter = ({ texts, speed }) => {
  const [currentText, setCurrentText] = useState('');
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    const intervalId = setInterval(() => {
      if (currentText.length < texts[currentIndex].length) {
        setCurrentText((prevText) => texts[currentIndex].substring(0, prevText.length + 1));
      } else {
        clearInterval(intervalId);
        setTimeout(() => {
          setCurrentText('');
          setCurrentIndex((prevIndex) => (prevIndex + 1) % texts.length);
        }, speed);
      }
    }, 100); // Adjust the typing speed

    return () => clearInterval(intervalId);
  }, [currentText, currentIndex, texts, speed]);

  return (
    <div>
      <p>{currentText}</p>
    </div>
  );
};

export default Typewriter;