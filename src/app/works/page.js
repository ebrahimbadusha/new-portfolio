
import Image from 'next/image';
export default function Home() {

	return(<div className="">
	
	<div id="site-border-left"></div>
	<div id="site-border-right"></div>
	<div id="site-border-top"></div>
	<div id="site-border-bottom"></div>
	 {/* <!-- Add your content of header --> */}
	<header>
	  <nav className="navbar  navbar-fixed-top navbar-default">
	    <div className="container">
		<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
		  <span className="sr-only">Toggle navigation</span>
		  <span className="icon-bar"></span>
		  <span className="icon-bar"></span>
		  <span className="icon-bar"></span>
		</button>
	
	      <div className="collapse navbar-collapse" id="navbar-collapse">
		<ul className="nav navbar-nav ">
		  <li><a href="../" title="">01 : Home</a></li>
		  <li><a href="./works" title="">02 : Works</a></li>
		  <li><a href="./about" title="">03 : About me</a></li>
		  <li><a href="./contact" title="">04 : Contact</a></li>
		 {/* <!-- <li><a href="./components" title="">05 : Components</a></li>--> */}
		</ul>
	
	
	      </div> 
	    </div>
	  </nav>
	</header>
	
	<div className="section-container">
	  <div className="container">
	    <div className="row">
	      
	      <div className="col-sm-8 col-sm-offset-2 section-container-spacer">
		<div className="text-center">
		  <h1 className="h2">02 : Works</h1>
		  <p>All My works are Small projecte aimed to demonsrate my use of those specified techchnologies.</p>
		</div>
	      </div>
	
	      <div className="col-md-12">
	     
	      <div id="myCarousel" className="carousel slide projects-carousel">
		{/* <!-- Carousel items --> */}
		<div className="carousel-inner">
		    <div className="item active">
			<div className="row">
			    <div className="col-sm-4">
	
			      <a href="./work" title="" className="black-image-project-hover">
				<Image width={500} 
          height={300} 
          style={{ width: '100%' }} src="/work01-hover.jpg" alt="" className="img-responsive" />
			      </a>
			      <div className="card-container card-container-lg">
				<h4>001/006</h4>
				<h3>Google Docs Clone</h3>
				<p>The clone of Google Docs with React ,Socket io,Quill,mongoDb.</p>
				<a href="https://github.com/badushaebrahim/docsclone" title="" className="btn btn-default">Discover</a>
			      </div>
			    </div>
			    <div className="col-sm-4">
			      <a href="./work" title="" className="black-image-project-hover">
				<Image width={500} 
          height={300} 
          style={{ width: '100%' }} src="/work02-hover.jpg" alt="" className="img-responsive" />
			      </a>
			      <div className="card-container card-container-lg">
				<h4>002/006</h4>
				<h3>React chat App</h3>
				<p>Superchat like App using react ,Firebase for instant communication.</p>
				<a href="https://github.com/badushaebrahim/Hello" title="" className="btn btn-default">Discover</a>
			      </div>
			    </div>
			    <div className="col-sm-4">
			      <a href="./work" title="" className="black-image-project-hover">
				<Image width={500} 
          height={300} 
          style={{ width: '100%' }} src="/work03-hover.jpg" alt="" className="img-responsive" />
			      </a>
			      <div className="card-container card-container-lg">
				<h4>003/006</h4>
				<h3>Student Teacher web portal</h3>
				<p>Full LAMP stack Site for Examination and atendence.</p>
				<a href="https://github.com/badushaebrahim/mini-project" title="" className="btn btn-default">Discover</a>
			      </div>
			    </div>
			    
			</div>
			{/* <!--/row--> */}
		    </div>
		    {/* <!--/item--> */}
		    <div className="item">
			<div className="row">
			  <div className="col-sm-4">
			    <a href="./work" className="black-image-project-hover">
			      <Image width={500} 
          height={300} 
          style={{ width: '100%' }} src="/work02-hover.jpg" alt="Image" className="img-responsive" />
			    </a>
			    <div className="card-container">
			      <h4>004/006</h4>
			      <h3>Whatsapp colne with flutter and firebase</h3>
			      <p>Whatsapp clone with flutter,firebase .</p>
			      <a href="./work" className="btn btn-default">Discover</a>
			    </div>
			  </div>
			   <div className="col-sm-4">
			      <a href="./work" className="black-image-project-hover">
				<Image width={500} 
          height={300} 
          style={{ width: '100%' }} src="/work01-hover.jpg" alt="Image" className="img-responsive" />
			      </a>
			      <div className="card-container">
				<h4>005/006</h4>
				<h3>Excesise assistant</h3>
				<p>Excexise asistant using a Opencv,Mediapipe,python using Using computer vission.</p>
				<a href="./work" className="btn btn-default">Discover</a>
			      </div>
			    </div>
			    
			    <div className="col-sm-4">
			      <a href="./work" className="black-image-project-hover">
				<Image width={500} 
          height={300} 
          style={{ width: '100%' }} src="/work03-hover.jpg" alt="Image" className="img-responsive" />			      </a>
			      <div className="card-container">
				<h4>006/006</h4>
				<h3>Android App for hand tracking</h3>
				<p>using AI to track hand and use data for those purposes.</p>
				<a href="./work" className="btn btn-default">Discover</a>
			      </div>
			    </div>
			    
			</div>
			{/* <!--/row--> */}
		    </div>
		    
		    {/* <!--/item--> */}
		</div>
		{/* <!--/carousel-inner-->  */}
		<a className="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
	
		<a className="right carousel-control" href="#myCarousel" data-slide="next">›</a>
	     </div>
	
	
	
	    {/* <!--/myCarousel--> */}
	    </div>
	
	
	
	    </div>
	  </div>
	</div>
	
	
	<footer className="footer-container text-center">
	  <div className="container">
	    <div className="row">
	      <div className="col-xs-12">
		<p>  | Website created Ebrahim Badusha O A ||<a href="">Get</a></p>
	      </div>
	    </div>
	  </div>
	</footer>
	
	</div>)


}