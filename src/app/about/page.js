import Image from "next/image"
import { SpeedInsights } from "@vercel/speed-insights/next"
export default function Home() {

	return(<div className="">
	<SpeedInsights/>
	<div id="site-border-left"></div>
	<div id="site-border-right"></div>
	<div id="site-border-top"></div>
	<div id="site-border-bottom"></div>
	 {/* <!-- Add your content of header --> */}
	<header>
	  <nav className="navbar  navbar-fixed-top navbar-default">
	    <div className="container">
		<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
		  <span className="sr-only">Toggle navigation</span>
		  <span className="icon-bar"></span>
		  <span className="icon-bar"></span>
		  <span className="icon-bar"></span>
		</button>
	
	      <div className="collapse navbar-collapse" id="navbar-collapse">
		<ul className="nav navbar-nav ">
		  <li><a href="./" title="">01 : Home</a></li>
		  <li><a href="./works" title="">02 : Works</a></li>
		  <li><a href="./about" title="">03 : About me</a></li>
		  <li><a href="./contact" title="">04 : Contact</a></li>
		 {/* <!-- <li><a href="./components" title="">05 : Components</a></li>--> */}
		</ul>
	
	
	      </div> 
	    </div>
	  </nav>
	</header>
	
	<div className="section-container">
	    <div className="container">
	      <div className="row">
		<div className="col-xs-12">
		  <div className="section-container-spacer text-center">
		    <h1 className="h2">01 : About me</h1>
		  </div>
		  
		  <div className="row">
		    <div className="col-md-10 col-md-offset-1">
		      <div className="row">
			<div className="col-xs-12 col-md-6">
			  <h3>Ebrahim Badusha O A</h3>
			  <p><b>Hi</b>, I am Ebrahim Badusha O A just like to build things that help me grow.</p>
			  <h3>Aim </h3>
			  Be able to build Scalable app or services for use in Business,Entertainment,etc
			    and be able to educate others in tech
			  <h3>Sills</h3>
			  <p>
			  My Skills include web(HTML,JS,REACT),Flutter,MYSQL,NOSQL(FIREBASE,MONGODB),
			  Android Development(Java,Kotlin) .</p>
			</div>
			<div className="col-xs-12 col-md-6">
			  <Image  width={500} 
          height={300} 
          style={{ width: '100%' }} src="/EBPIC.jpg" className="img-responsive" alt="" />
			</div>
		      </div>
		    </div>
		  </div>
		  
		  
	       </div>
	      </div>
	    </div>
	  </div>
	
	
	  <footer className="footer-container text-center">
	  <div className="container">
	    <div className="row">
	      <div className="col-xs-12">
	       <p>  | Website created Ebrahim Badusha O A ||<a href="">Get</a></p>
	      </div>
	    </div>
	  </div>
	</footer>
	
	  </div>)
}