
export default function Home() {

	return(<div className="">
	<div id="site-border-left"></div>
	<div id="site-border-right"></div>
	<div id="site-border-top"></div>
	<div id="site-border-bottom"></div>
	 {/* <!-- Add your content of header --> */}
	<header>
	  <nav className="navbar  navbar-fixed-top navbar-default">
	    <div className="container">
		<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
		  <span className="sr-only">Toggle navigation</span>
		  <span className="icon-bar"></span>
		  <span className="icon-bar"></span>
		  <span className="icon-bar"></span>
		</button>
	
	      <div className="collapse navbar-collapse" id="navbar-collapse">
		<ul className="nav navbar-nav ">
		  <li><a href="./" title="">01 : Home</a></li>
		  <li><a href="./works" title="">02 : Works</a></li>
		  <li><a href="./about" title="">03 : About me</a></li>
		  <li><a href="./contact" title="">04 : Contact</a></li>
		 {/* <!-- <li><a href="./components" title="">05 : Components</a></li>--> */}
		</ul>
	
	
	      </div> 
	    </div>
	  </nav>
	</header>
	
	<div className="section-container">
	    <div className="container">
	      <div className="row">
		<div className="col-xs-12">
		  <div className="section-container-spacer text-center">
		    <h1 className="h2">03 : Contact me</h1>
		  </div>
		  
		  <div className="row">
		    <div className="col-md-10 col-md-offset-1">
		       <form action="" className="reveal-content">
			  <div className="row">
			    <div className="col-md-7">
			      <div className="form-group">
				<input type="email" className="form-control" id="email" placeholder="Email" />
			      </div>
			      <div className="form-group">
				<input type="text" className="form-control" id="subject" placeholder="Subject" />
			      </div>
			      <div className="form-group">
				<textarea className="form-control" rows="5" placeholder="Enter your message"></textarea>
			      </div>
			      <button type="submit" className="btn btn-default btn-lg">Send</button>
			    </div>
			    <div className="col-md-5 address-container">
			      <ul className="list-unstyled">
				<li>
				  <span className="fa-icon">
				    <i className="fa fa-phone" aria-hidden="true"></i>
				  </span>
				  + 91 95446 55941
				</li>
				<li>
				  <span className="fa-icon">
				    <i className="fa fa-at" aria-hidden="true"></i>
				  </span>
				  baduhaebrahim62@mail.com
				</li>
				<li>
				  <span className="fa-icon">
				    <i className="fa fa-at" aria-hidden="true"></i>
				  </span>
				  baduhaebrahim44@mail.com
				</li>
				<li>
				  <span className="fa-icon">
				    <i className="fa fa fa-map-marker" aria-hidden="true"></i>
				  </span>
				  
				</li>
			      </ul>
			      <h3>Follow me on social networks</h3>
			      <a href="https://www.facebook.com/badushaorapra.sinan/" title="" className="fa-icon" >
				<i className="fa fa-facebook"></i>
			      </a>
			      <a href="https://twitter.com/7651a501d8cc492" title="" className="fa-icon">
				<i className="fa fa-twitter"></i>
			      </a>
			      <a href="https://www.linkedin.com/in/ebrahim-badusha-8115a0174" title="" className="fa-icon">
				<i className="fa fa-linkedin"></i>
			      </a>
			      <a href=":https://github.com/badushaebrahim/badushaebrahim-profile" title="" className="fa-icon">
				<i className="fa fa-github"></i>
			      </a>
			    </div>
			  </div>
			</form>
		    </div>
	
		  </div>
		 
	       </div>
	      </div>
	    </div>
	  </div>
	
	
	<footer className="footer-container text-center">
	  <div className="container">
	    <div className="row">
	      <div className="col-xs-12">
	       <p>  | Website created Ebrahim Badusha O A ||<a href="">Get</a></p>
	      </div>
	    </div>
	  </div>
	</footer>
	
	</div>);
}