
"use client"
import styles from "./page.module.css";
import { SpeedInsights } from "@vercel/speed-insights/next"
import Typewriter from "./components/typewriter";
export default function Home() {
  const texts = [ "I'm Ebrahim Badusha O A","MicroService Developer" ,  "A Web Developer"];
  return (
    <div className="minimal">
    <SpeedInsights/>
<div id="site-border-left"></div>
<div id="site-border-right"></div>
<div id="site-border-top"></div>
<div id="site-border-bottom"></div>
{/* <!-- Add your content of header --> */}
<header>
  <nav className="navbar  navbar-fixed-top navbar-inverse">
    <div className="container">
        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
          <span className="sr-only">Toggle navigation</span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
        </button>

      <div className="collapse navbar-collapse" id="navbar-collapse">
        <ul className="nav navbar-nav ">
          <li><a href="./" title="">01 : Home</a></li>
          <li><a href="./works" title="">02 : Works</a></li>
          <li><a href="./about" title="">03 : About me</a></li>
          <li><a href="./contact" title="">04 : Contact</a></li>
         {/* <!-- <li><a href="./components" title="">05 : Components</a></li>--> */}
        </ul>


      </div>
    </div>
  </nav>
</header>
{/* <!-- Add your site or app content here --> */}
  <div className="hero-full-container background-image-container white-text-container backgroundImageEb " >
    <div className="container">
      <div className="row">
        <div className="col-xs-12">
          <div className="hero-full-wrapper">
            <div className="text-content">
              <h1>Hello,<br/>
                <span id="typed-strings"><br/>
                  {/* <span>I'm Ebrahim Badusha O A</span>
                  <span>A Web Developer</span>
                  <span>A Android Developer</span>
                  <span>A flutter Developer</span> */}
                 <Typewriter texts={texts} speed={600}/>
                </span>
                <span id="typed"></span>
              </h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
{/* <script>
  document.addEventListener("DOMContentLoaded", function (event) {
     type();
     movingBackgroundImage();
  });
</script> */}


</div>
  );
}
